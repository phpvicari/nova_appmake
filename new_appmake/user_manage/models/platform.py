from django.db import models

class Platform(models.Model):
    name = models.CharField(max_length=50)
    url = models.URLField(max_length=200)
    created = models.DateTimeField('date created', auto_now_add=True)
    updated = models.DateTimeField('last updated', auto_now_add=True, auto_now=True)

    def __str__(self):
        return self.name