from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.html import escape, mark_safe

class User(AbstractUser):
    is_client = models.BooleanField(default=True)

    class Meta:
        app_label = 'user_manage'

class Client(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    cpf = models.CharField(max_length=14)
    phone = models.CharField(max_length=15)

    def __str__(self):
        return self.user.username