from django.db import models

class Store(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    name = models.CharField(max_length=100)
    url = models.URLField(max_length=200)
    key1 = models.CharField(max_length=200)
    key2 = models.CharField(max_length=200)
    created = models.DateTimeField('date created', auto_now_add=True)
    updated = models.DateTimeField('last updated', auto_now_add=True, auto_now=True)

    def __str__(self):
        return self.name